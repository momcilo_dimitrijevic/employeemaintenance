﻿using EmployeeMaintenance.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace EmployeeMaintenance.DI
{
    public static class EntityFrameworkExtension
    {
        public static void AddEntityFramework(this IServiceCollection services, string connectionString)
        {
            services.AddDbContext<EmployeeMaintenanceContext>(options =>
                options.UseSqlServer(connectionString));
        }
    }
}
