﻿using EmployeeMaintenance.Persistence.Repositories;
using EmployeeMaintenance.Service.Services;
using Microsoft.Extensions.DependencyInjection;

namespace EmployeeMaintenance.DI
{
    public static class CommonConventionExtension
    {
        public static void RegisterTypes(this IServiceCollection services)
        {
            services.Scan(scan => scan
                .FromAssemblyOf<PersonRepository>()
                .AddClasses()
                .AsMatchingInterface()
                .WithScopedLifetime());

            services.Scan(scan => scan
                .FromAssemblyOf<PersonService>()
                .AddClasses()
                .AsMatchingInterface()
                .WithScopedLifetime());
        }
    }
}
