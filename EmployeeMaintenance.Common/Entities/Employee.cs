﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EmployeeMaintenance.Common.Entities
{
    public class Employee
    {
        [Key]
        public int EmployeeId { get; set; }

        public int PersonId { get; set; }

        [Required]

        [MaxLength(16)]
        public string EmployeeNum { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Column(TypeName = "Date")]
        public DateTime EmployedDate { get; set; }

        [DataType(DataType.Date)]
        [Column(TypeName = "Date")]
        public DateTime? TerminatedDate { get; set; }

        
        public Person Person { get; set; }
    }
}
