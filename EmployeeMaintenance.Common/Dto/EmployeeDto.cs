﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EmployeeMaintenance.Common.Dto
{
    public class EmployeeDto
    {
        public int EmployeeId { get; set; }
        public string EmployeeNum { get; set; }
        public DateTime EmployedDate { get; set; }
        public DateTime? TerminatedDate { get; set; }

        public PersonDto Person { get; set; }
    }
}
