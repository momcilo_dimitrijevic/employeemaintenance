﻿using System;
using System.Collections.Generic;
using System.Text;
using EmployeeMaintenance.Common.Dto;

namespace EmployeeMaintenance.Common.Interfaces
{
    public interface IEmployeeService
    {
        List<EmployeeDto> GetAll();
        EmployeeDto GetById(int id);
    }
}
