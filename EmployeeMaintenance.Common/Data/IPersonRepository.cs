﻿using EmployeeMaintenance.Common.Entities;

namespace EmployeeMaintenance.Common.Data
{
    public interface IPersonRepository : IRepository<Person>
    {
    }
}
