﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace EmployeeMaintenance.Common.Data
{
    public interface IRepository<T>
    {
        void Create(T entity);

        T GetById(params object[] id);

        void Delete(params object[] id);

        void Update(T entity, params object[] id);

        List<T> GetAll();

        T GetSingle(Expression<Func<T, bool>> exp);
    }
}
