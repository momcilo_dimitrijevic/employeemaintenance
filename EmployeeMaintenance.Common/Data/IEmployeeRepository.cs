﻿using EmployeeMaintenance.Common.Entities;

namespace EmployeeMaintenance.Common.Data
{
    public interface IEmployeeRepository : IRepository<Employee>
    {
    }
}
