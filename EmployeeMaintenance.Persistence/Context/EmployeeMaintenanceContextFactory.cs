﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace EmployeeMaintenance.Persistence.Context
{
    public class EmployeeMaintenanceContextFactory : IDesignTimeDbContextFactory<EmployeeMaintenanceContext>
    {
        public EmployeeMaintenanceContext CreateDbContext(string[] args)
        {
            var configuration = GetConfiguration(args);
            var connectionString = configuration.GetConnectionString("DefaultConnection");

            var builder = new DbContextOptionsBuilder<EmployeeMaintenanceContext>();
            builder.UseSqlServer(connectionString);

            return new EmployeeMaintenanceContext(builder.Options);
        }

        private static IConfiguration GetConfiguration(string[] args)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddCommandLine(args)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            return builder.Build();
        }
    }
}
