﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Entities = EmployeeMaintenance.Common.Entities;

namespace EmployeeMaintenance.Persistence.Context
{
    public class EmployeeMaintenanceContext : DbContext
    {
        public EmployeeMaintenanceContext(DbContextOptions<EmployeeMaintenanceContext> options)
            : base(options)
        {
        }

        public DbSet<Entities.Person> Persons { get; set; }
        public DbSet<Entities.Employee> Employees { get; set; }
    }
}
