﻿using EmployeeMaintenance.Common.Data;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using EmployeeMaintenance.Persistence.Context;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace EmployeeMaintenance.Persistence.Repositories
{
    public abstract class BaseRepository<TEntity> : IRepository<TEntity>
        where TEntity : class
    {
        protected EmployeeMaintenanceContext DbContext { get; }

        protected BaseRepository(EmployeeMaintenanceContext dbContext)
        {
            DbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public void Create(TEntity entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));

            DbContext.Set<TEntity>().Add(entity);
            DbContext.SaveChanges();
        }

        public void Delete(params object[] id)
        {
            var entity = GetById(id);
            if (entity == null) throw new NullReferenceException(nameof(entity));

            DbContext.Set<TEntity>().Remove(entity);
            DbContext.SaveChanges();
        }

        public List<TEntity> GetAll()
        {
            return DbContext.Set<TEntity>().AsNoTracking().ToList();
        }

        public TEntity GetById(params object[] id)
        {
            return DbContext.Set<TEntity>().Find(id);
        }

        public void Update(TEntity entity, params object[] id)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));

            var original = GetById(id);
            if (original == null) throw new NullReferenceException(nameof(original));

            DbContext.Entry(original).CurrentValues.SetValues(entity);

            DbContext.SaveChanges();
        }

        public TEntity GetSingle(Expression<Func<TEntity, bool>> exp)
        {
            return DbContext.Set<TEntity>().AsNoTracking().SingleOrDefault(exp);
        }
    }
}
