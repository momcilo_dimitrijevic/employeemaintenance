﻿using EmployeeMaintenance.Common.Data;
using EmployeeMaintenance.Common.Entities;
using EmployeeMaintenance.Persistence.Context;

namespace EmployeeMaintenance.Persistence.Repositories
{
    public class PersonRepository : BaseRepository<Person>, IPersonRepository
    {
        public PersonRepository(EmployeeMaintenanceContext dbContext) : base(dbContext)
        {
        }
    }
}
