﻿using EmployeeMaintenance.Common.Data;
using EmployeeMaintenance.Common.Entities;
using EmployeeMaintenance.Persistence.Context;

namespace EmployeeMaintenance.Persistence.Repositories
{
    public class EmployeeRepository : BaseRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(EmployeeMaintenanceContext dbContext) : base(dbContext)
        {
        }
    }
}
