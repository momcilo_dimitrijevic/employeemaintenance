﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeeMaintenance.Common.Interfaces;
using EmployeeMaintenance.Web.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Storage;

namespace EmployeeMaintenance.Web.Controllers
{
    public class EmployeesController : Controller
    {
        private readonly IEmployeeService _employeeService;

        public EmployeesController(IEmployeeService employeeService)
        {
            _employeeService = employeeService;
        }


        [HttpGet]
        public IActionResult Index()
        {
            var allEmployees = _employeeService.GetAll();
            var model = allEmployees.Select(x => new EmployeeViewModel()
            {
                FirstName = x.Person.FirstName,
                LastName = x.Person.LastName,
                BirthDate = x.Person.BirthDate,
                EmployeeNum = x.EmployeeNum,
                EmployedDate = x.EmployedDate,
                EmployeeId = x.EmployeeId,
                TerminatedDate = x.TerminatedDate
            }).ToList();

            return View(model);
        }

        [HttpGet]
        public IActionResult Edit(int? id)
        {
            EmployeeViewModel model = null;

            if (id.HasValue)
            {
                var employee = _employeeService.GetById(id.Value);
                if (employee == null)
                    return NotFound();
            }
            else
            {
                model = new EmployeeViewModel();
            }

            return View(model);
        }

        [HttpPost]
        public IActionResult Delete(int id)
        {
            return View();
        }
    }
}