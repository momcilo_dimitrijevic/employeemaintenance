﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EmployeeMaintenance.Common.Data;
using EmployeeMaintenance.Common.Dto;
using EmployeeMaintenance.Common.Entities;
using EmployeeMaintenance.Common.Interfaces;

namespace EmployeeMaintenance.Service.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IPersonRepository _personRepository;

        public EmployeeService(IEmployeeRepository employeeRepository, IPersonRepository personRepository)
        {
            _employeeRepository = employeeRepository;
            _personRepository = personRepository;
        }

        public List<EmployeeDto> GetAll()
        {
            var employees = _employeeRepository.GetAll();
    
            //TODO: Automapper for mapping object between layers
            return employees?.Select(MapEntityToDto).ToList(); ;
        }

        public EmployeeDto GetById(int id)
        {
            var employee = _employeeRepository.GetById(id);

            return MapEntityToDto(employee);
        }

        private EmployeeDto MapEntityToDto(Employee entity)
        {
            if (entity == null)
                return null;

            var dto = new EmployeeDto()
            {
                EmployeeId = entity.EmployeeId,
                EmployeeNum = entity.EmployeeNum,
                EmployedDate = entity.EmployedDate,
                TerminatedDate = entity.TerminatedDate,
                Person = new PersonDto()
                {
                    FirstName = entity.Person.FirstName,
                    LastName = entity.Person.LastName,
                    BirthDate = entity.Person.BirthDate,
                    PersonId = entity.Person.PersonId
                }
            };

            return dto;
        }
    }
}
